#!/bin/bash
echo "开始卸载 铜豌豆 中文套件 ..."

apt -y purge  \
baidunetdisk \
netease-cloud-music \
sogoupinyin \
electronic-wechat \
youdao-dict \
wps-office wps-office-fonts

apt -y purge linuxqq
apt -y purge deepin.com.qq.im:i386

apt -y purge debian-cn-fonts
apt -y purge fonts-zh-cn-misc-atzlinux

echo "开始卸载不需要使用的中文软件依赖包 ..."
apt -y autoremove

echo '清理铜豌豆软件包安装源 ...'
apt-add-repository -r 'deb http://118.24.9.73/debian/ stable main non-free'
apt-add-repository -r 'deb http://debian-cn.lilishare.com/debian/ stable main non-free'
apt-add-repository -r 'deb https://apt.atzlinux.com/debian buster  main contrib non-free'
rm -fv /var/lib/apt/lists/*118.24.9.73*
rm -fv /var/lib/apt/lists/*debian-cn.lilishare.com*
rm -fv /var/lib/apt/lists/*apt.atzlinux.com*
apt update

echo "卸载完成！欢迎再次使用铜豌豆：https://www.atzlinux.com"
