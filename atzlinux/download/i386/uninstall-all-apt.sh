#!/bin/bash
echo "开始卸载 铜豌豆 中文套件 ..."

apt -y purge  \
deepin.com.qq.im \
sogoupinyin \
electronic-wechat \
youdao-dict \
wps-office wps-office-fonts

apt -y purge debian-cn-fonts
apt -y purge fonts-zh-cn-misc-atzlinux

echo "开始卸载不需要使用的中文软件依赖包 ..."
apt -y autoremove

echo "《铜豌豆 Linux》欢迎您的下次光临！ https://www.atzlinux.com "
./uninstall-debian-cn-repo.sh
