#!/bin/bash
apt -y install wget
wget -c -O atzlinux-archive-keyring_lastest_all.deb https://www.atzlinux.com/atzlinux/pool/main/a/atzlinux-archive-keyring/atzlinux-archive-keyring_lastest_all.deb
apt -y install ./atzlinux-archive-keyring_lastest_all.deb
dpkg --add-architecture i386
apt update
apt -y purge glx-diversions
apt -y install libgl1-mesa-dri:i386 libgl1-mesa-glx:i386 libc6:i386 libgl1-mesa-dri:amd64 libgl1-mesa-glx:amd64
apt autoremove
apt -y install steam-launcher
echo "
在 应用程序 -- 游戏 菜单启动。

第一次启动时，需要联网下载相关文件到用户目录下，请耐心等待。

程序界面有中文，在 “设置 -- 界面”选择使用 简体中文。

注：steam 游戏平台需要账号才能够登录，如果之前没有 steam 账号，请先到如下网址注册：

https://store.steampowered.com/join/?&snr=1_60_4__62
"
