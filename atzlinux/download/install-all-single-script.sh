#!/bin/bash
apt -y install wget
wget -c -O atzlinux-archive-keyring_lastest_all.deb https://www.atzlinux.com/atzlinux/pool/main/a/atzlinux-archive-keyring/atzlinux-archive-keyring_lastest_all.deb
apt -y install ./atzlinux-archive-keyring_lastest_all.deb
dpkg --add-architecture i386
apt update
apt -y install xdg-utils
apt -y install xfce4-settings
apt -y install libcanberra-gtk-module
apt -y install libcanberra-gtk-module:i386
apt -y install electronic-wechat
apt -y install linuxqq
apt -y install \
fcitx-config-common \
fcitx-config-gtk \
fcitx-frontend-all \
fcitx-frontend-qt5 \
fcitx-googlepinyin \
fcitx-m17n \
fcitx-module-x11 \
fcitx-sunpinyin \
fcitx-table-wubi \
fcitx-table-wbpy \
fcitx-ui-classic
apt -y install sogoupinyin
rm -f /etc/apt/sources.list.d/sogoupinyin.list
apt -y install fonts-zh-cn-misc-atzlinux
apt -y install desktop-file-utils
apt -y install baidunetdisk
apt -y install netease-cloud-music
apt -y install youdao-dict
apt -y install wps-office wps-office-fonts
apt -y install atzlinux-store
echo "安装成功，请退出当前登录，重新登录，让安装生效。"
